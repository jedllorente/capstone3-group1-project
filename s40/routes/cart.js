const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cart');
const auth = require('../auth');

// - Add item to cart
router.post('/addToCart', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    let data = {
        
        name: req.body.cartItems.name,
        product: req.body.cartItems.product,
        quantity: req.body.cartItems.quantity,
        price : req.body.cartItems.price,
        cartItems : req.body.cartItems,
        userId: userData.id,
        productId: req.body.productId
    }
    
    if(!userData.isAdmin){
        cartController.addToCart(data).then(resultFromController =>
        res.send(resultFromController));
    }else{
        res.send("you're an admin, you're not allowed to add item to cart")
    }
})

// display all cart (Admin only)
router.get('/getAllCart', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin){
        cartController.allCart(data).then(resultFromController =>
        res.send(resultFromController));
    }else{
        res.send("you're not admin, you're not allowed to check all the cart")
    }
})

//retrieve authenticated user cart
router.get('/getCart' , auth.verify , (req, res)=> {

    const userData = auth.decode(req.headers.authorization)

    let data = {
        userId: userData.id     
    }
    if(!userData.isAdmin){
        cartController.getCart(data).then(resultFromController =>
        res.send(resultFromController));
    }else{
        res.send("you're an admin")
    }
})


// checkout cartitem 
router.post('/checkout', auth.verify, (req,res) => {
    const userData = auth.decode(req.headers.authorization)

    let data = {

        userId: userData.id ,
        cartId: req.body.cartId,
        totalAmount: req.body.totalAmount
    }
    if(!userData.isAdmin){
        cartController.checkout(data).then(resultFromController =>
        res.send(resultFromController));
    }else{
        res.send("you're an admin")
    }

})



// remote cart item
router.put('/remove', auth.verify, (req,res)=> {

    const userData = auth.decode(req.headers.authorization)

    let data = {

        cartId: req.body.cartId,
        userId: userData.id
    
    }
    if(!userData.isAdmin){
        cartController.removeItem(data).then(resultFromController =>
        res.send(resultFromController));
    }else{
        res.send("you're an admin")
    }

})


// route for increasing quantity
router.put('/add', auth.verify, (req,res)=> {

    const userData = auth.decode(req.headers.authorization)

    let data = {

        cartItems : req.body.cartItems,
        userId: userData.id
    
    }
    if(!userData.isAdmin){
        cartController.addQuantity(data).then(resultFromController =>
        res.send(resultFromController));
    }else{
        res.send("you're an admin")
    }

})


// route for decreasing quantity
router.put('/minus', auth.verify, (req,res)=> {

    const userData = auth.decode(req.headers.authorization)

    let data = {

        cartItems : req.body.cartItems,
        userId: userData.id
    
    }
    if(!userData.isAdmin){
        cartController.minusQuantity(data).then(resultFromController =>
        res.send(resultFromController));
    }else{
        res.send("you're an admin")
    }

})


// user Order Details
router.get('/getUserOrder', auth.verify, (req,res)=> {

    const userData = auth.decode(req.headers.authorization)

    let data = {

        userId: userData.id
    }

    if(!userData.isAdmin){
        cartController.getUserOrder(data).then(resultFromController =>
        res.send(resultFromController));
    }else{
        res.send("you're an admin")
    }

})

module.exports = router;